import React from 'react';
import './Home.css'

function Home() {
    return(
        <>
            <div className='homeTitle'>
                AustinEats
            </div>
            <div className='homeImage'>

            </div>
            <div className='homeSubTitle'>
                Supporting local Austin businesses, one at a time.
            </div>
            <div className='homeSubSubTitle'>
                Explore your favorite recipes, from all over the world, from your favorite restaurants.
            </div>

        </>
    )
}

export default Home;