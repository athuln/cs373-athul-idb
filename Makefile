# sourced from Texas Votes: https://gitlab.com/forbesye/fitsbits/-/tree/c8bbdbbcee676cb57f7533beb84f47de784289de/

front-update:
	cd front-end/ && npm install

front-start:
	cd front-end/ && npm start

front-build:
	cd front-end/ && npm build
