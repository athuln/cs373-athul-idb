# AustinEats

Welcome to the GitLab repository for AustinEats! <br>

**About:** AustinEats is a lightweight, multi-platform website helping Austin residents find local restaurants, explore different cuisines, and try recipes from their favorite restaurants. We hope to inspire Austinites to support more local businesses, try different types of food, and learn how to make some of their favorite meals.

## Group Members

| Name            | UT EID  | GitLab ID       |
| --------------- | ------- | --------------- |
| Thomas Moore    | tcm2448 | @tmooretcm      |
| Athul Nair      | an26748 | @athuln         |
| Mihika Birmiwal | mb63689 | @mihikabirmiwal |
| Christian Camp  | cdc4459 | @Repoised       |
| Clint Camp      | cac8697 | @xXDarkEyesXx   |

## Links

Our website (prod): https://www.austineats.me/ <br>
Our website (dev) : https://main.d3bl3ypxabjsn.amplifyapp.com/ <br>
Postman           : https://documenter.getpostman.com/view/23508831/2s83tJGW4m <br>
Pipelines         : https://gitlab.com/mihikabirmiwal/cs373-idb/-/pipelines <br>

## Git SHA

Phase I:  b52aff4780b259e178c53bf7eb5972d0aa35953b

The project leader for Phase I was Mihika Birmiwal (@mihikabirmiwal).

## Completion Time

### Phase I

| Name            | Estimated Hours | Actual Hours |
| --------------- | :-------------: | :----------: |
| Thomas Moore    | 15              | 22           |
| Athul Nair      | 20              | 25           |
| Mihika Birmiwal | 20              | 25           |
| Christian Camp  | 20              | 23           |
| Clint Camp      | 20              | 21           |

## Comments

README formatting, GitLab pipeline structure, and front-end code adapted from [FindingFootprints](https://gitlab.com/AlejandroCantu/group2), [TexasVotes](https://gitlab.com/forbesye/fitsbits/-/tree/master/), and our TA mentor Canyon Mooney.

